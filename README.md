secwrap -- a UID/GID sandboxing system for cPanel servers
---------------------------------------------------------

## Background

When asked about sandboxing plugins in cPanel, so that resellers do not
get effective root access, they have this to say:

> Essentially, the administrative interface of cPanel, the WHM, is designed
> to run as root. Changing this design decision would require a complete
> rewrite of the WHM code. For this reason, code executed via the WHM must
> operate in a secure fashion. cPanel works diligently to ensure the necessary
> security considerations are met in code that we produce. However, we cannot
> guarantee the efforts of third parties.

With `secwrap`, we have demonstrated that it is possible to provide sandboxing
of third-party plugins and cPanel internal functionality without requiring a
redesign.

As a bonus, `secwrap` should provide the same protections to any other panel,
but will need some custom configuration.

## cPanel responds to version 0.1 release of `secwrap`

When cPanel found out about `secwrap` version 0.1, cPanel CEO Nick Koston
personally contacted us and had this to say:

> The secwrap code you published was recently brought to our attention, and I
> wanted to take a moment to clarify our position:
>
> * If your goal is to provide a true sandbox ala http://code.google.com/p/chromium/wiki/LinuxSUIDSandbox,
>   this is not feasible as it would largely defeat the purpose of allowing
>   plugins to WHM.
> * If your goal is to run WHM plugins with the UID of the reseller, this
>   [is] quite feasible, and we can certainly provide this as an option.

As such, future work is planned to further strengthen the sandboxing by introducing
a permissions manager and further sandboxing using the seccomp(2) and capsicum
frameworks.

## Installation

```
# make
# make install enable
```

## Turning off the sandboxing

```
# make disable
```

## Uninstallation

```
# make uninstall
```
