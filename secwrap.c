/*
 * secwrap
 *
 * Wrap CGI interpreters to drop privileges if it is running as root
 * for a reseller based on the REMOTE_USER CGI field.
 *
 * Copyright (c) 2013 Rack911.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <grp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/queue.h>
#include <pwd.h>		/* struct passwd, getpwnam() */

#ifdef __FreeBSD__
# include <sys/sysctl.h>	/* sysctl() */
#endif

#include "confparse.h"

#ifndef MAXPATHLEN
# define MAXPATHLEN		4096
#endif

#define CONFIG_PATH		"/etc/secwrap.conf"

#ifdef DEBUG
# define DPRINTF(x, ...)	fprintf(stderr, "debug: " x "\n", __VA_ARGS__)
#else
# define DPRINTF(x, ...)
#endif

struct script_path {
	LIST_ENTRY(script_path) node;
	char path[MAXPATHLEN];
};

LIST_HEAD(script_path_list, script_path);

struct interpreter {
	LIST_ENTRY(interpreter) node;
	char *name;
	char *path;
	struct script_path_list whitelist;
};

LIST_HEAD(interpreter_list, interpreter) interp_list;

/*
 * find_executable_name
 *
 * Finds executable name, using readlink(2) on Linux.
 *
 * input        - none
 * output       - executable name
 * side effects - none
 */
const char *
find_executable_name(void)
{
	static char procname[FILENAME_MAX];

#if defined(linux)
	int len;

	memset(procname, 0, sizeof procname);

	len = readlink("/proc/self/exe", procname, FILENAME_MAX - 1);
	if (len <= 0)
		return NULL;
	procname[len] = '\0';
#elif defined(__FreeBSD__)
	int mib[4];

	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_PATHNAME;
	mib[3] = -1;

	size_t cb = sizeof(procname);
	sysctl(mib, 4, procname, &cb, NULL, 0);
#endif

	return procname;
}

/*
 * script_whitelist_add
 *
 * Adds the script (or parent dir) to the whitelist.
 *
 * input        - interpreter, path
 * output       - none
 * side effects - structure 'script_path' is allocated, script_path.path is initialized
 *                with the contents of `path`, and added to `script_whitelist`.
 */
void
script_whitelist_add(struct interpreter *interp, const char *path)
{
	struct script_path *pe;

	pe = calloc(sizeof(*pe), 1);

	/* NOTE: strlcpy() is not implemented on glibc, this is close enough due to the
	 *       calloc usage above.  If we change to a different memory allocation method,
	 *       this should be rechecked for security.
	 *       (or, better yet, glibc could implement strlcpy().  that'd be cool, too.)
	 */
	strncpy(pe->path, path, MAXPATHLEN - 1);

	LIST_INSERT_HEAD(&interp->whitelist, pe, node);
}

/*
 * interpreter_add
 *
 * Adds an interpreter to the interpreter list.
 *
 * input        - interpreter name, interpreter path
 * output       - interpreter object
 * side effects - none
 */
struct interpreter *
interpreter_add(const char *name, const char *path)
{
	struct interpreter *interp;

	interp = calloc(sizeof(*interp), 1);
	interp->name = strdup(name);
	interp->path = strdup(path);

	LIST_INSERT_HEAD(&interp_list, interp, node);

	return interp;
}

/*
 * interpreter_find
 *
 * Finds an interpreter by pathname.
 *
 * input        - path to lookup
 * output       - interpreter else NULL
 * side effects - none
 */
struct interpreter *
interpreter_find(const char *path)
{
	struct interpreter *it;

	for (it = interp_list.lh_first; it != NULL; it = it->node.le_next)
	{
		DPRINTF("check: %s (%s)", it->name, it->path);

		if (!strcmp(it->path, path))
			return it;
	}

	return NULL;
}

/*
 * interpreter_exceptions_parse
 *
 * Parse a list of interpreter exceptions.
 *
 * input        - config entry to parse, interpreter object
 * output       - none
 * side effects - an interpreter object is loaded with exceptions
 */
void
interpreter_exceptions_parse(config_entry_t *parent, struct interpreter *it)
{
	config_entry_t *ce;

	for (ce = parent->ce_entries; ce != NULL; ce = ce->ce_next)
		script_whitelist_add(it, ce->ce_varname);
}

/*
 * interpreter_parse
 *
 * Parse an interpreter config entry.
 *
 * input        - config entry to parse
 * output       - none
 * side effects - an interpreter object is created
 */
void
interpreter_parse(config_entry_t *parent)
{
	config_entry_t *ce;
	struct interpreter *it;
	char *name = NULL, *path = NULL;

	/* first, find the name and path */
	name = parent->ce_vardata;
	for (ce = parent->ce_entries; ce != NULL; ce = ce->ce_next)
	{
		if (!strcasecmp(ce->ce_varname, "path"))
			path = ce->ce_vardata;
	}

	if (name == NULL || path == NULL)
	{
		fprintf(stderr, "config: interpreter{} block without name or path\n");
		return;
	}

	DPRINTF("name = %s, path = %s", name, path);

	it = interpreter_add(name, path);

	/* now, add exceptions to the interpreter */
	for (ce = parent->ce_entries; ce != NULL; ce = ce->ce_next)
	{
		if (!strcasecmp(ce->ce_varname, "exceptions"))
			interpreter_exceptions_parse(ce, it);
	}
}

/*
 * conf_load
 *
 * Loads a configuration file.
 *
 * input        - path to config file
 * output       - none
 * side effects - various structures are created by the config file
 */
void
conf_load(const char *path)
{
	config_file_t *cf = config_load(path);
	config_entry_t *ce;

	for (ce = cf->cf_entries; ce != NULL; ce = ce->ce_next)
	{
		if (!strcasecmp(ce->ce_varname, "interpreter"))
			interpreter_parse(ce);
	}

	config_free(cf);
}

/*
 * file_exists
 *
 * Checks if a file exists.
 *
 * input        - path to the file
 * output       - true if the file exists, else false
 * side effects - none
 */
bool
file_exists(const char *path)
{
	struct stat st;
	int res;

	res = stat(path, &st);
	if (res < 0)
	{
		DPRINTF("stat(2) on %s failed: %s", path, strerror(errno));
		return false;
	}

	if (!S_ISREG(st.st_mode))
	{
		DPRINTF("rejecting %s as non-regular file", path);
		return false;
	}

	return true;
}

/*
 * drop_privileges
 *
 * Drop privileges to the UID owned by `username`.
 *
 * input        - username
 * output       - true if successful, false if not.
 * side effects - process effective UID is changed
 */
bool
drop_privileges(const char *username)
{
	struct passwd *pwd;

	pwd = getpwnam(username);
	if (pwd == NULL)
	{
		DPRINTF("could not find /etc/passwd entry for %s", username);
		return false;
	}

	/* drop group first, then uid */
	if (setgid(pwd->pw_gid) < 0)
	{
		DPRINTF("setgid(%d) failed: %s", pwd->pw_gid, strerror(errno));
		return false;
	}

	if (setegid(pwd->pw_gid) < 0)
	{
		DPRINTF("setgid(%d) failed: %s", pwd->pw_gid, strerror(errno));
		return false;
	}

	if (setgroups(0, NULL) < 0)
	{
		DPRINTF("setgroups() failed: %s", strerror(errno));
		return false;
	}

	if (setuid(pwd->pw_uid) < 0)
	{
		DPRINTF("setuid(%d) failed: %s", pwd->pw_uid, strerror(errno));
		return false;
	}

	if (seteuid(pwd->pw_uid) < 0)
	{
		DPRINTF("seteuid(%d) failed: %s", pwd->pw_uid, strerror(errno));
		return false;
	}

	return true;
}

/*
 * should_drop_privileges
 *
 * Checks whether or not it is necessary to drop privileges.
 *
 * input        - requested username, script filename
 * output       - true if we should drop privileges, else false
 * side effects - none
 */
bool
should_drop_privileges(struct interpreter *interp, const char *username, const char *script)
{
	struct script_path *pe;

	/* if our EUID is non-root, we cannot change our EUID anyway, so don't bother. */
	if (geteuid() != 0)
		return false;

	if (username == NULL || !strcasecmp(username, "root"))
		return false;

	/* we do not have SCRIPT_FILENAME, it is better to drop privileges here. */
	if (script == NULL)
		return true;

	for (pe = interp->whitelist.lh_first; pe != NULL; pe = pe->node.le_next)
	{
		DPRINTF("check: %s", pe->path);

		if (!strcmp(script, pe->path) || !strncmp(script, pe->path, strlen(pe->path)))
		{
			DPRINTF("check: %s matched %s", pe->path, script);
			return false;
		}
	}

	return true;
}

/*
 * main
 *
 * Program entrypoint.
 *
 * input        - commandline arguments (length and vector)
 * output       - exit code
 * side effects - if successful, INTERPRETER is run after dropping privileges
 */
void cpwrap_main(void) __attribute__((constructor));

void
cpwrap_main(void)
{
	const char *username = getenv("REMOTE_USER");
	const char *script   = getenv("SCRIPT_FILENAME");
	const char *interp   = find_executable_name();
	struct interpreter *it;

	if (file_exists(CONFIG_PATH))
	{
		DPRINTF("config (%s) exists, loading it", CONFIG_PATH);
		conf_load(CONFIG_PATH);
	}

	DPRINTF("we are running interpreter: %s", interp);

	it = interpreter_find(interp);
	if (it == NULL)
	{
		DPRINTF("no interpreter object found for: %s", interp);
		return;
	}

	DPRINTF("interpreter object: %s", it->name);

	if (username != NULL)
	{
		DPRINTF("REMOTE_USER is '%s'", username);
	}

	if (script != NULL)
	{
		DPRINTF("SCRIPT_FILENAME is '%s'", script);
	}

	if (should_drop_privileges(it, username, script))
	{
		if (!drop_privileges(username))
		{
			fprintf(stderr, "failed to drop privileges to %s, aborting\n", username);
			exit(EXIT_FAILURE);
		}
	}

	DPRINTF("privileges dropped successfully, euid=%d/egid=%d", geteuid(), getegid());
}
