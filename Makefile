PRELOAD = secwrap.so

SRCS = confparse.c	secwrap.c
OBJS = ${SRCS:.c=.o}

CFLAGS = -O2 -ggdb3 -Wall -Wextra -pedantic -std=gnu99

.c.o:
	${CC} -fPIC ${CFLAGS} -c -o $@ $<

${PRELOAD}: ${OBJS}
	${CC} -shared -fPIC -o $@ ${OBJS}

clean:
	rm ${OBJS} ${PRELOAD}

install:
	install -d -m755 ${DESTDIR}/usr/local/lib
	install -m755 ${PRELOAD} ${DESTDIR}/usr/local/lib/${PRELOAD}
	install -d -m755 ${DESTDIR}/etc
	install -m644 secwrap.conf ${DESTDIR}/etc/secwrap.conf

uninstall:
	rm -f ${DESTDIR}/usr/local/lib/${PRELOAD}
	rm -f ${DESTDIR}/etc/secwrap.conf

enable:
	echo /usr/local/lib/${PRELOAD} > /etc/ld.so.preload

disable:
	grep -v ${PRELOAD} /etc/ld.so.preload > /etc/ld.so.preload.new; exit 0
	mv /etc/ld.so.preload.new /etc/ld.so.preload

.SUFFIXES: .o
